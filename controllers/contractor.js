import db from "../config/db.js";
import { nanoid } from "nanoid";
import { formatDate } from "../helpers/global.js";

// 1. GET payment count by user id - Frontend: ConOverview.jsx
export const getPaymentCountById = (req, res) => {
  const userId = req.params.userId;

  const query = `SELECT COUNT(paymentId) as paymentCount, status FROM payments WHERE userId = ? GROUP BY status`;

  db.query(query, userId, (error, result) => {
    if (error) {
      return res.status(400).json({
        error: "Database query could not be completed.",
      });
    }
    if (result.length > 0) {
      return res.status(200).json(result);
    } else {
      return res
        .status(200)
        .json({ message: "User does not have any payments in the database" });
    }
  });
};

// 2. GET all payments based on payment status and user id - Frontend: ConPendingPayments.jsx and ConPaymentHistory.jsx
export const getPaymentsByStatusAndUserId = (req, res) => {
  const { userId, status } = req.params;
  let query;

  if (status === "pending") {
    query = `SELECT * FROM payments WHERE userId = ? AND (status = "pending" OR status = "declined")`;
  } else {
    query = `SELECT * FROM payments WHERE userId = ? AND (status = "paid" OR status = "in progress")`;
  }

  db.query(query, userId, (error, result) => {
    if (error) {
      return res.status(400).json({
        error: "Database query could not be completed.",
      });
    }

    if (result.length > 0) {
      res.status(200).json(result);
    } else {
      return res.status(200).json({
        message:
          "User does not have any payments of this type in the database.",
      });
    }
  });
};

// 3. GET payment request details by payment id - Frontend: ConPaymentDetail.jsx
export const getPaymentDetail = (req, res) => {
  const paymentId = req.params.paymentId;

  const query = `SELECT payments.*, users.firstName, users.lastName FROM payments LEFT JOIN users ON users.userId = payments.reviewedBy WHERE payments.paymentId = ?`;

  db.query(query, paymentId, (error, result) => {
    if (error) {
      return res
        .status(400)
        .json({ error: "Database query could not be completed." });
    }
    if (result.length > 0) {
      return res.status(200).json(result);
    } else {
      return res.status(400).json({ error: "Payment not found." });
    }
  });
};

// 4. POST a new payment request - Frontend: ConPaymentRequest.jsx
export const submitPaymentRequest = (req, res) => {
  const userId = req.params.userId;
  const { status, hoursWorked, description, payrollDate, currency, bonus } =
    req.body;

  // Checks for required fields
  if (!status || !hoursWorked || !description || !payrollDate || !currency) {
    return res.status(400).json({ error: "Missing required field(s)." });
  }

  // creates paymentId, formats paydate and creates request date
  const paymentId = nanoid();
  const payDate = payrollDate.slice(0, 10);
  const requestDate = formatDate();

  const query = `INSERT INTO payments (paymentId, userId, requestDate, status, hoursWorked, description, payrollDate, currency, bonus) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)`;

  db.query(
    query,
    [
      paymentId,
      userId,
      requestDate,
      status,
      hoursWorked,
      description,
      payDate,
      currency,
      bonus,
    ],
    (error, result) => {
      if (error) {
        return res
          .status(400)
          .json({ error: "Database query could not be completed." });
      } else {
        return res.status(201).json({
          message: "Payment request successfully submitted.",
        });
      }
    }
  );
};

// 5. PATCH profile - Frontend: ConProfile.jsx
export const updateContractorProfile = (req, res) => {
  const userId = req.params.userId;
  const { email, company, country, city, phone, currency } = req.body;

  const query = `UPDATE users SET email = ?, company = ?, country = ?, city = ?, phone = ?, currency = ? WHERE userId = ?`;

  db.query(
    query,
    [email, company, country, city, phone, currency, userId],
    (error, result) => {
      if (error) {
        res
          .status(400)
          .json({ error: "Database query could not be completed." });
      } else {
        res.status(200).json(result);
      }
    }
  );
};

// 6. DELETE a payment request by payment id - Frontend: ConPendingPayments.jsx
export const deletePaymentRequest = (req, res) => {
  const paymentId = req.params.paymentId;

  const query = `DELETE FROM payments WHERE paymentId = ?`;

  db.query(query, paymentId, (error, result) => {
    if (error) {
      return res
        .status(400)
        .json({ error: "Database query could not be completed." });
    }

    if (result.affectedRows > 0) {
      return res
        .status(200)
        .json({ message: "Payment request has been successfully deleted." });
    } else {
      return res
        .status(404)
        .json({ message: "Payment does not exist and could not be deleted." });
    }
  });
};
